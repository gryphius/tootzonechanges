#!/bin/bash

INSTALLPATH=/opt/tootzonechanges/
go build -o $INSTALLPATH/tootzonechanges .

cp tootzonechanges.service /etc/systemd/system 
systemctl daemon-reload

systemctl restart tootzonechanges

package main

import (
	"fmt"
	"os"
	"time"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

var PREVSTATE *ParentInfo
var CURRENTSTATE *ParentInfo

const SLEEP_INTERVAL = 1 * time.Hour
const TOOT_PAUSE = 1 * time.Second
const STATE_FILE = "zonestate.json"
const MAX_CHANGES_PER_RUN = 30

func main() {
	wd, _ := os.Getwd()
	fmt.Println("Starting up, working directory: ", wd)
	loadConfig("botconfig.json")
	loop()
}

func logTime() string {
	currentTime := time.Now()
	return currentTime.Format("2006-01-02 15:04:05")
}

func loop() {
	startup := true // don't sleep on the first run
	for {
		if startup {
			startup = false
		} else {
			fmt.Println(logTime(), "Sleeping for ", SLEEP_INTERVAL, ". Cu in a bit.")
			time.Sleep(SLEEP_INTERVAL)
		}

		// load prev state
		storeddata, err := LoadState(STATE_FILE)
		if err == nil {
			PREVSTATE, err = FromJSON(storeddata)
			if err != nil {
				fmt.Println(logTime(), "state file corrupt: ", err, "resetting.")
				PREVSTATE = NewParentInfo()
			}
		} else {
			fmt.Println(logTime(), "could not load statefile: ", err, "- initializing new state")
			PREVSTATE = NewParentInfo()
		}

		fmt.Println(logTime(), "state serial: ", PREVSTATE.Serial)
		// load current state
		fmt.Println(logTime(), "Loading new zone...")
		CURRENTSTATE, err := FromXFR(CONFIG.XfrServer, CONFIG.XfrPort)
		if err != nil {
			fmt.Println(logTime(), "load/xfr zone failed: ", err)
		} else {
			fmt.Println(logTime(), "New state serial: ", CURRENTSTATE.Serial)
			jsondata, err := ToJSON(CURRENTSTATE)
			if err != nil {
				fmt.Println(logTime(), "Could not marshal new state: ", err)
				continue
			}
			err = SaveState(STATE_FILE, jsondata)
			if err != nil {
				fmt.Println(logTime(), "Could not save new state: ", err)
				continue
			}
			//backup for debugging
			// backupFile := fmt.Sprintf("state-%v.json", CURRENTSTATE.Serial)
			// SaveState(backupFile, jsondata)

			// at this point the file contains the new state and on the next run will be loaded as PREVSTATE
			fmt.Println(logTime(), "New state stored successfully - checking for differences...")

			changes := CURRENTSTATE.changes(PREVSTATE)
			if len(changes) > MAX_CHANGES_PER_RUN {
				fmt.Println(logTime(), "uh oh - we have ", len(changes), " changes which is more than the configured maximum ", MAX_CHANGES_PER_RUN, " - skipping this changeset")
				continue
			}
			if len(changes) == 0 {
				fmt.Println(logTime(), "no changes detected")
				continue
			}
			for _, change := range changes {
				message := change.TootMessage()
				fmt.Println(logTime(), "Toot:")
				fmt.Println("---")
				fmt.Println(message)
				fmt.Println("---")
				status, err := toot(message)
				if err != nil {
					fmt.Println(logTime(), "Posting status failed: ", err)
				} else {
					fmt.Println(logTime(), "Posted status with ID ", status.ID)
				}
			}
		}

	}
}

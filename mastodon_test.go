package main

import (
	"context"
	"fmt"
	"log"
	"testing"

	"github.com/mattn/go-mastodon"
)

// not actual tests, just snippets to test the mastodon API

func DisTestRegisterApp(t *testing.T) {
	loadConfig("botconfig-mastodns.json")
	app, err := mastodon.RegisterApp(context.Background(), &mastodon.AppConfig{
		Server:     CONFIG.Server,
		ClientName: "tootzonechanges",
		Scopes:     "read write follow",
		Website:    "https://gitlab.com/gryphius/tootzonechanges",
	})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("client-id    : %s\n", app.ClientID)
	fmt.Printf("client-secret: %s\n", app.ClientSecret)
}

func DisTestTootSingle(t *testing.T) {
	loadConfig("botconfig-mastodns.json")

	message := "Test toot from the bot - hang on while we're getting ready to post the real thing"
	fmt.Println(logTime(), "Toot:")
	fmt.Println("---")
	fmt.Println(message)
	fmt.Println("---")
	status, err := toot(message)
	if err != nil {
		fmt.Println(logTime(), "Posting status failed: ", err)
	} else {
		fmt.Println(logTime(), "Posted status with ID ", status.ID)
	}

}

package main

import (
	"net"
	"strings"

	"github.com/miekg/dns"
)

type Service struct {
	Server string
	Key    string
	Secret string
	Port   string
}

func NewService(server string, port string) *Service {
	return &Service{
		Server: server,
		Port:   port,
	}
}

func (s *Service) ServerPort() (string, error) {

	port := s.Port
	if port == "" {
		port = "53"
	}

	sp := s.Server
	if strings.IndexByte(s.Server, ':') < 0 {
		sp = s.Server + ":" + port
	}

	n, p, err := net.SplitHostPort(sp)
	if err != nil {
		return "", err
	}

	h, err := net.LookupHost(n)
	if err != nil {
		return "", err
	}

	return net.JoinHostPort(h[0], p), nil
}

func (s *Service) Transfer(zone string) ([]dns.RR, error) {
	m := new(dns.Msg)
	m.SetAxfr(zone)

	h, err := s.ServerPort()
	if err != nil {
		return nil, err
	}

	tr := new(dns.Transfer)
	a, err := tr.In(m, h)
	if err != nil {
		return nil, err
	}

	var res []dns.RR
	for ex := range a {
		if ex.Error != nil {
			return nil, ex.Error
		}
		res = append(res, ex.RR...)
	}

	return res, nil
}

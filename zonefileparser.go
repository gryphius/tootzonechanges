package main

import (
	"os"
	"strings"

	"github.com/miekg/dns"
)

func ParseZoneFile(path string) ([]dns.RR, error) {
	b, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	check(err)
	zp := dns.NewZoneParser(b, ".", "")
	var res []dns.RR
	for rr, ok := zp.Next(); ok; rr, ok = zp.Next() {
		res = append(res, rr)
	}
	if err := zp.Err(); err != nil {
		return res, err
	}
	return res, nil
}

func ParseZoneFileString(data string) ([]dns.RR, error) {
	zp := dns.NewZoneParser(strings.NewReader(data), ".", "")
	var res []dns.RR
	for rr, ok := zp.Next(); ok; rr, ok = zp.Next() {
		res = append(res, rr)
	}
	if err := zp.Err(); err != nil {
		return res, err
	}
	return res, nil
}

package main

import (
	"context"

	"github.com/mattn/go-mastodon"
)

func toot(message string) (*mastodon.Status, error) {
	c := mastodon.NewClient(&mastodon.Config{
		Server:       CONFIG.Server,
		ClientID:     CONFIG.ClientID,
		ClientSecret: CONFIG.ClientSecret,
	})
	err := c.Authenticate(context.Background(), CONFIG.Email, CONFIG.Password)
	if err != nil {
		return nil, err
	}
	status, err := c.PostStatus(context.Background(), &mastodon.Toot{Status: message})
	return status, err

}

package main

import (
	"encoding/json"
	"fmt"
	"os"
	"reflect"
	"sort"
	"strings"

	"github.com/miekg/dns"
	"golang.org/x/net/idna"
)

type DsInfo struct {
	Algorithm uint8  `json:"alg"`
	Keytag    uint16 `json:"keytag"`
	HashAlgo  uint8  `json:"hashalg"`
}

func (ds *DsInfo) toString() string {
	return fmt.Sprint(ds.Keytag, " ", ds.Algorithm, " ", ds.HashAlgo)
}

type ChildInfo struct {
	Ns map[string]bool    `json:"ns"`
	Ds map[string]*DsInfo `json:"ds"`
}

func NewChildInfo() *ChildInfo {
	childinfo := &ChildInfo{
		Ns: make(map[string]bool),
		Ds: make(map[string]*DsInfo),
	}
	return childinfo
}

type KeyInfo struct {
	Algorithm uint8  `json:"alg"`
	Flags     uint16 `json:"flags"`
	Keytag    uint16 `json:"keytag"`
}

func (ki *KeyInfo) toString() string {
	return fmt.Sprint(ki.Keytag, " ", ki.Flags, " ", ki.Algorithm)
}

type ParentInfo struct {
	Serial   uint32                     `json:"serial"`
	Dnskeys  map[string]*KeyInfo        `json:"dnskey"` // we use maps for fast lookups
	Children map[string]*ChildInfo      `json:"childinfo"`
	Glue     map[string]map[string]bool `json:"glue"`
}

type ChangeType int64

const (
	ADD_CHILD ChangeType = iota
	REMOVE_CHILD
	MODIFY_CHILD_NS
	ADD_DNSKEY
	REMOVE_DNSKEY
	MODIFY_CHILD_DS
	MODIFY_GLUE
)

type Change struct {
	Type      ChangeType
	Target    string
	Message   string
	ChangeSet []string
}

var idnaDisplayProfile = idna.Display

func (c *Change) TootMessage() string {
	var sb strings.Builder
	emoji := GetEmoji(c.Target)
	if emoji != "" {
		sb.WriteString(emoji)
		sb.WriteString(" ")
	}

	isIDNA := false

	decodedTarget, err := idnaDisplayProfile.ToUnicode(c.Target)
	if err == nil && decodedTarget != c.Target {
		isIDNA = true
	}

	if isIDNA {
		sb.WriteString(decodedTarget)
		sb.WriteString(" ( ")
	}
	sb.WriteString(c.Target)
	if isIDNA {
		sb.WriteString(" )")
	}
	sb.WriteString(" : ")
	sb.WriteString(c.Message)
	sb.WriteString("\n")
	for _, s := range c.ChangeSet {
		sb.WriteString(s)
		sb.WriteString("\n")
	}
	return sb.String()
}

func NewParentInfo() *ParentInfo {
	return &ParentInfo{
		Serial:   0,
		Children: make(map[string]*ChildInfo),
		Glue:     make(map[string]map[string]bool),
		Dnskeys:  make(map[string]*KeyInfo),
	}
}

func ToJSON(p *ParentInfo) ([]byte, error) {
	b, err := json.MarshalIndent(p, "", "    ")
	if err != nil {
		fmt.Printf("Error: %s", err)
		return nil, err
	}
	return b, nil
}

func FromJSON(jsonBlob []byte) (*ParentInfo, error) {
	parentinfo := NewParentInfo()
	err := json.Unmarshal(jsonBlob, &parentinfo)
	return parentinfo, err
}

func FromXFR(server string, port string) (*ParentInfo, error) {
	parentinfo := NewParentInfo()
	serv := NewService(server, port)
	rr, err := serv.Transfer(".")
	if err != nil {
		return nil, err
	}
	parentinfo.Fill(rr)
	return parentinfo, nil
}
func FromZonefile(path string) (*ParentInfo, error) {
	parentinfo := NewParentInfo()
	rr, err := ParseZoneFile(path)
	if err != nil {
		return nil, err
	}
	parentinfo.Fill(rr)
	return parentinfo, nil
}

func FromZonefileString(data string) (*ParentInfo, error) {
	parentinfo := NewParentInfo()
	rr, err := ParseZoneFileString(data)
	if err != nil {
		return nil, err
	}
	parentinfo.Fill(rr)
	return parentinfo, nil
}

func (p *ParentInfo) Fill(rr []dns.RR) {
	for _, r := range rr {
		childName := r.Header().Name

		switch x := r.(type) {
		case *dns.SOA:
			p.Serial = x.Serial
		case *dns.NS:
			p.addNS(childName, x.Ns)
		case *dns.DNSKEY:
			keyinfo := &KeyInfo{Algorithm: x.Algorithm, Keytag: x.KeyTag(), Flags: x.Flags}
			p.Dnskeys[keyinfo.toString()] = keyinfo
		case *dns.A:
			p.addGlue(childName, x.A.String())

		case *dns.AAAA:
			p.addGlue(childName, x.AAAA.String())

		case *dns.DS:
			p.addDS(childName, x.Algorithm, x.KeyTag, x.DigestType)
		}
	}
}

func LoadState(path string) ([]byte, error) {
	return os.ReadFile(path)
}
func SaveState(path string, content []byte) error {
	return os.WriteFile(path, content, 0644)
}

func (p *ParentInfo) addGlue(name string, ipaddr string) {
	if p.Glue[name] == nil {
		p.Glue[name] = make(map[string]bool)
	}
	p.Glue[name][ipaddr] = true
}
func (p *ParentInfo) addNS(childname string, nsname string) {
	if p.Children[childname] == nil {
		p.Children[childname] = NewChildInfo()
	}
	p.Children[childname].Ns[nsname] = true
}
func (p *ParentInfo) addDS(childname string, algorithm uint8, keytag uint16, hashalgo uint8) {
	if p.Children[childname] == nil {
		p.Children[childname] = NewChildInfo()
	}
	newDS := &DsInfo{
		Algorithm: algorithm,
		Keytag:    keytag,
		HashAlgo:  hashalgo,
	}
	p.Children[childname].Ds[newDS.toString()] = newDS
}

func (current *ParentInfo) changes(previous *ParentInfo) []Change {
	var changes []Change

	//if the serial didn't increase we're not gonna bother, also if prev serial is 0 we assume
	//this was an empty state and do not calculate differences
	if current.Serial <= previous.Serial || previous.Serial == 0 {
		return changes
	}

	currentChildren := current.ChildrenNames()
	prevChildren := previous.ChildrenNames()

	// removed DNSKEY
	var removedDNSKEY []string
	removed_zsk_only := true
	removed_ksk_only := true
	for _, dnskey := range previous.Dnskeys {
		if current.Dnskeys[dnskey.toString()] == nil {
			removedDNSKEY = append(removedDNSKEY, dnskey.toString())
			if dnskey.Flags != 256 {
				removed_zsk_only = false
			}
			if dnskey.Flags != 257 {
				removed_ksk_only = false
			}
		}
	}
	if len(removedDNSKEY) > 0 {
		keyinfo := ""
		if removed_ksk_only {
			keyinfo = "(KSK)"
		}
		if removed_zsk_only {
			keyinfo = "(ZSK)"
		}
		message := "DNSKEY removed " + keyinfo
		changes = append(changes, Change{Type: REMOVE_DNSKEY, Target: ".", Message: message, ChangeSet: removedDNSKEY})
	}

	// added DNSKEY
	var addedDNSKEY []string
	added_zsk_only := true
	added_ksk_only := true
	for _, dnskey := range current.Dnskeys {
		if previous.Dnskeys[dnskey.toString()] == nil {
			addedDNSKEY = append(addedDNSKEY, dnskey.toString())
			if dnskey.Flags != 256 {
				added_zsk_only = false
			}
			if dnskey.Flags != 257 {
				added_ksk_only = false
			}
		}
	}
	if len(addedDNSKEY) > 0 {
		keyinfo := ""
		if added_ksk_only {
			keyinfo = "(KSK)"
		}
		if added_zsk_only {
			keyinfo = "(ZSK)"
		}
		message := "DNSKEY added " + keyinfo
		changes = append(changes, Change{Type: ADD_DNSKEY, Target: ".", Message: message, ChangeSet: addedDNSKEY})
	}

	//removed TLD
	for _, child := range prevChildren {
		if current.Children[child] == nil {
			changes = append(changes, Change{Type: ADD_CHILD, Target: child, Message: "Delegation removed"})
		}
	}

	for _, child := range currentChildren {
		//added TLD
		if previous.Children[child] == nil {
			changes = append(changes, Change{Type: ADD_CHILD, Target: child, Message: "New Delegation"})
			continue
		}

		// added NS
		myns := current.Nameservers(child)
		var addedNS []string
		var sameNS []string
		for _, ns := range myns {
			if !previous.Children[child].Ns[ns] {
				addedNS = append(addedNS, ns)
			} else {
				sameNS = append(sameNS, ns)
			}
		}

		// removed NS
		prevns := previous.Nameservers(child)
		var removedNS []string
		for _, ns := range prevns {
			if !current.Children[child].Ns[ns] {
				removedNS = append(removedNS, ns)
			}
		}
		if len(addedNS) > 0 || len(removedNS) > 0 {
			var nsDisplay []string

			for _, remDS := range removedNS {
				nsDisplay = append(nsDisplay, "- "+remDS)
			}
			for _, same := range sameNS {
				nsDisplay = append(nsDisplay, "  "+same)
			}
			for _, newDS := range addedNS {
				nsDisplay = append(nsDisplay, "+ "+newDS)
			}

			nsMessage := "Changed NS"

			changes = append(changes, Change{Type: MODIFY_CHILD_NS, Target: child, Message: nsMessage, ChangeSet: nsDisplay})
		}

		// added DS
		myds := current.DSRecords(child)
		var addedDS []string
		var sameDS []string
		for _, ds := range myds {
			if previous.Children[child].Ds[ds] == nil {
				addedDS = append(addedDS, ds)
			} else {
				sameDS = append(sameDS, ds)
			}
		}
		// removed DS
		prevds := previous.DSRecords(child)
		var removedDS []string
		for _, ds := range prevds {
			if current.Children[child].Ds[ds] == nil {
				removedDS = append(removedDS, ds)
			}
		}
		if len(addedDS) > 0 || len(removedDS) > 0 {
			var dsDisplay []string

			for _, remDS := range removedDS {
				dsDisplay = append(dsDisplay, "- "+remDS)
			}
			for _, same := range sameDS {
				dsDisplay = append(dsDisplay, "  "+same)
			}
			for _, newDS := range addedDS {
				dsDisplay = append(dsDisplay, "+ "+newDS)
			}

			dsMessage := "Changed DS"
			if current.HasSameKeyTags(previous, child) {
				dsMessage = "Changed DS digest algorithm"
			} else {
				dsMessage = "Key Rollover - Changed DS"
			}
			if current.IsAlgoRollover(previous, child) {
				dsMessage = "Algorithm Rollover - Changed DS"
			}
			if len(current.Children[child].Ds) == 0 {
				dsMessage = "Going insecure - Removed all DS"
			}
			if len(previous.Children[child].Ds) == 0 {
				dsMessage = "Going secure - Added new DS"
			}

			changes = append(changes, Change{Type: MODIFY_CHILD_DS, Target: child, Message: dsMessage, ChangeSet: dsDisplay})
		}

	}

	//added and removed ips
	gluenames := make([]string, 0, len(current.Glue))
	for k := range current.Glue {
		gluenames = append(gluenames, k)
	}
	for _, gluename := range gluenames {
		var addedIps []string
		var removedIps []string

		ipsNewMap := current.Glue[gluename]
		ipsnew := make([]string, 0, len(ipsNewMap))
		for k := range ipsNewMap {
			ipsnew = append(ipsnew, k)
		}

		ipsPrevMap := previous.Glue[gluename]
		if ipsPrevMap == nil {
			continue
		}
		ipsprev := make([]string, 0, len(ipsPrevMap))
		for k := range ipsPrevMap {
			ipsprev = append(ipsprev, k)
		}

		var sameIPs []string
		for _, ip := range ipsnew {
			if !ipsPrevMap[ip] {
				addedIps = append(addedIps, ip)
			} else {
				sameIPs = append(sameIPs, ip)
			}
		}
		for _, ip := range ipsprev {
			if !ipsNewMap[ip] {
				removedIps = append(removedIps, ip)
			}
		}

		if len(addedIps) > 0 || len(removedIps) > 0 {
			var ipDisplay []string

			for _, remIP := range removedIps {
				ipDisplay = append(ipDisplay, "- "+remIP)
			}
			for _, same := range sameIPs {
				ipDisplay = append(ipDisplay, "  "+same)
			}
			for _, newIP := range addedIps {
				ipDisplay = append(ipDisplay, "+ "+newIP)
			}

			ipMessage := "Changed Glue"

			changes = append(changes, Change{Type: MODIFY_GLUE, Target: gluename, Message: ipMessage, ChangeSet: ipDisplay})
		}

	}

	return changes
}

func (p *ParentInfo) ChildrenNames() []string {
	keys := make([]string, 0, len(p.Children))
	for k := range p.Children {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	return keys
}

func (p *ParentInfo) Nameservers(child string) []string {
	nsnames := make([]string, 0, len(p.Children[child].Ns))
	for k := range p.Children[child].Ns {
		nsnames = append(nsnames, k)
	}
	sort.Strings(nsnames)
	return nsnames
}

func (p *ParentInfo) DSRecords(child string) []string {
	dsrecs := make([]string, 0, len(p.Children[child].Ds))
	for k := range p.Children[child].Ds {
		dsrecs = append(dsrecs, k)
	}
	sort.Strings(dsrecs)
	return dsrecs
}

func (p *ParentInfo) IsAlgoRollover(p2 *ParentInfo, child string) bool {
	type void struct{}
	var member void
	algoset := make(map[uint8]void)

	for _, ds := range p.Children[child].Ds {
		algoset[ds.Algorithm] = member
	}
	for _, ds := range p2.Children[child].Ds {
		algoset[ds.Algorithm] = member
	}
	return len(algoset) > 1
}

// HasSameKeyTags returns true if the two parents have the same keytags for the given child
func (p *ParentInfo) HasSameKeyTags(p2 *ParentInfo, child string) bool {
	type void struct{}
	var member void
	mykeytags := make(map[uint16]void)
	otherkeytags := make(map[uint16]void)

	for _, ds := range p.Children[child].Ds {
		mykeytags[ds.Keytag] = member
	}

	for _, ds := range p2.Children[child].Ds {
		otherkeytags[ds.Keytag] = member
	}
	return reflect.DeepEqual(mykeytags, otherkeytags)
}

module leetdreams.ch/toot-zone-changes/v2

go 1.21

toolchain go1.22.3

require (
	github.com/mattn/go-mastodon v0.0.8
	github.com/miekg/dns v1.1.59
	golang.org/x/net v0.25.0
)

require (
	github.com/gorilla/websocket v1.5.1 // indirect
	github.com/tomnomnom/linkheader v0.0.0-20180905144013-02ca5825eb80 // indirect
	golang.org/x/mod v0.17.0 // indirect
	golang.org/x/sync v0.7.0 // indirect
	golang.org/x/sys v0.20.0 // indirect
	golang.org/x/text v0.15.0 // indirect
	golang.org/x/tools v0.21.0 // indirect
)

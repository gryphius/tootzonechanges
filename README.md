# Tootzonechanges

This bot toots on mastodon when it detects changes in the DNS root zone, such as:

 * Added / Removed Nameservers
 * Added / Removed DS Records
 * Glue record changes
 * Added / Removed TLD

The bot can be seen in action on https://mastodns.net/@diffroot  ( @diffroot@mastodns.net )

![screenshot](./screenshot.png)

## setup 

 * create a bot account on the desired mastodon server, and a client secret according to https://docs.joinmastodon.org/client/token/
 * optional - recommended: build a local secondary which IXFR's the root zone ( the bot does a full AXFR every hour, so it's nicer to get it from a local mirror )
 * create botconfig.json according to the example from botconfig.example.json
 * run the bot: `go run .`

## install as a service
 * create a directory for the bot, for example `/opt/tootzonechanges`
 * copy `botconfig.json` into the working dir
 * build the binary: `go build -o tootzonechanges .` 
 * create a service file (example in `tootzonechanges.service`) , copy to `/etc/systemd/system`, run `systemctl daemon-reload`
 * `systemctl start tootzonechanges`


package main

import (
	"strings"
	"testing"
)

func TestReadZoneFile(t *testing.T) {
	info, err := FromZonefile("testdata/simpletestzone1.zone")
	if err != nil {
		t.Error(err)
	}
	if info.Serial != 2022043000 {
		t.Error("Expected serial 2022043000, got ", info.Serial)
	}
}

func TestRegression_002(t *testing.T) {
	zone_old := `
.                       86400   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 1 1800 900 604800 86400
xn--imr513n.		86400	IN	DS	14747 8 2 6581FD2FC61A9A81926E5DBE6F7EEB940402BF2DB280C98F50ABA666E4C74371

`

	zone_new := `
.                       86400   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2 1800 900 604800 86400
xn--imr513n.		86400	IN	DS	12187 8 2 CA358CB59005AC2C0871A1C5D9D954C82A4077BD5507844C8FA7C86E8DCD5A64
xn--imr513n.		86400	IN	DS	14747 8 2 6581FD2FC61A9A81926E5DBE6F7EEB940402BF2DB280C98F50ABA666E4C74371
			`

	info1, err := FromZonefileString(zone_old)
	if err != nil {
		t.Error(err)
	}

	info2, _ := FromZonefileString(zone_new)
	if err != nil {
		t.Error(err)
	}

	changes := info2.changes(info1)
	if len(changes) != 1 {
		t.Error("Expected 1 change, got ", len(changes))
	}

	if info2.HasSameKeyTags(info1, "xn--imr513n.") {
		t.Error("Expected hasSameKeyTags to return false, got true")
	}

	change := changes[0]
	message := change.TootMessage()
	if !strings.Contains(message, `( xn--imr513n. ) : Key Rollover`) {
		t.Error("expected key rollover message for idn domain', got ", message)
	}

}

func TestDNSKEYChangeAddKSK(t *testing.T) {
	zone_old := `
.                       86400   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 1 1800 900 604800 86400
.       28167   IN      DNSKEY  257 3 8 AwEAAa96jeuknZlaeSrvyAJj6ZHv28hhOKkx3rLGXVaC6rXTsDc449/cidltpkyGwCJNnOAlFNKF2jBosZBU5eeHspaQWOmOElZsjICMQMC3aeHbGiShvZsx4wMYSjH8e7Vrhbu6irwCzVBApESjbUdpWWmEnhathWu1jo+siFUiRAAxm9qyJNg/wOZqqzL/dL/q8PkcRU5oUKEpUge71M3ej2/7CPqpdVwuMoTvoB+ZOT4YeGyxMvHmbrxlFzGOHOijtzN+u1TQNatX2XBuzZNQ1K+s2CXkPIZo7s6JgZyvaBevYtxPvYLw4z9mR7K2vaF18UYH9Z9GNUUeayffKC73PYc=
.       28167   IN      DNSKEY  256 3 8 AwEAAZ5A7jOztf62cGqhPhutjnyl7KBjIsjbyTb8il+FsgbMUbO2NQHaSbatHdlOlqANncDwSIKZ9ryqd1+Dy1PoGzeTUv95vOJnVVJHlJu7xdavnUmPs+Mh2NV7hDlTTwPn5uXgFxAaxoO9M/YIAC92GryCLjoJEg9JzeevkktEM/sFpmRv4I5jQtlLyRqVbnCzcWpi04XaVLxRKvURkd/Mdb/2RQS3MYvrkEBXuqtnAVBCf6Fx4sgBYOfYvbUuG2diLnGJW/MXvFpctZgQ76+3FwMqAZfR9k5bohL7AF3+jqz4MUiootYoh5koyt7VEnUULxxy6U5PINTGgOC26f3zZuk=

`

	zone_new := `
.                       86400   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2 1800 900 604800 86400
.       28167   IN      DNSKEY  257 3 8 AwEAAa96jeuknZlaeSrvyAJj6ZHv28hhOKkx3rLGXVaC6rXTsDc449/cidltpkyGwCJNnOAlFNKF2jBosZBU5eeHspaQWOmOElZsjICMQMC3aeHbGiShvZsx4wMYSjH8e7Vrhbu6irwCzVBApESjbUdpWWmEnhathWu1jo+siFUiRAAxm9qyJNg/wOZqqzL/dL/q8PkcRU5oUKEpUge71M3ej2/7CPqpdVwuMoTvoB+ZOT4YeGyxMvHmbrxlFzGOHOijtzN+u1TQNatX2XBuzZNQ1K+s2CXkPIZo7s6JgZyvaBevYtxPvYLw4z9mR7K2vaF18UYH9Z9GNUUeayffKC73PYc=
.       28167   IN      DNSKEY  257 3 8 AwEAAaz/tAm8yTn4Mfeh5eyI96WSVexTBAvkMgJzkKTOiW1vkIbzxeF3+/4RgWOq7HrxRixHlFlExOLAJr5emLvN7SWXgnLh4+B5xQlNVz8Og8kvArMtNROxVQuCaSnIDdD5LKyWbRd2n9WGe2R8PzgCmr3EgVLrjyBxWezF0jLHwVN8efS3rCj/EWgvIWgb9tarpVUDK/b58Da+sqqls3eNbuv7pr+eoZG+SrDK6nWeL3c6H5Apxz7LjVc1uTIdsIXxuOLYA4/ilBmSVIzuDWfdRUfhHdY6+cn8HFRm+2hM8AnXGXws9555KrUB5qihylGa8subX2Nn6UwNR1AkUTV74bU=
.       28167   IN      DNSKEY  256 3 8 AwEAAZ5A7jOztf62cGqhPhutjnyl7KBjIsjbyTb8il+FsgbMUbO2NQHaSbatHdlOlqANncDwSIKZ9ryqd1+Dy1PoGzeTUv95vOJnVVJHlJu7xdavnUmPs+Mh2NV7hDlTTwPn5uXgFxAaxoO9M/YIAC92GryCLjoJEg9JzeevkktEM/sFpmRv4I5jQtlLyRqVbnCzcWpi04XaVLxRKvURkd/Mdb/2RQS3MYvrkEBXuqtnAVBCf6Fx4sgBYOfYvbUuG2diLnGJW/MXvFpctZgQ76+3FwMqAZfR9k5bohL7AF3+jqz4MUiootYoh5koyt7VEnUULxxy6U5PINTGgOC26f3zZuk=
			`

	info1, err := FromZonefileString(zone_old)
	if err != nil {
		t.Error(err)
	}

	info2, _ := FromZonefileString(zone_new)
	if err != nil {
		t.Error(err)
	}

	changes := info2.changes(info1)
	if len(changes) != 1 {
		t.Error("Expected 1 change, got ", len(changes))
	}

	change := changes[0]
	message := change.TootMessage()
	if !strings.Contains(message, `. : DNSKEY added (KSK)`) {
		t.Error("expected key rollover message for added KSK. got ", message)
	}

}

func TestDNSKEYChangeRemoveZSK(t *testing.T) {
	zone_old := `
.                       86400   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 1 1800 900 604800 86400
.       28167   IN      DNSKEY  257 3 8 AwEAAa96jeuknZlaeSrvyAJj6ZHv28hhOKkx3rLGXVaC6rXTsDc449/cidltpkyGwCJNnOAlFNKF2jBosZBU5eeHspaQWOmOElZsjICMQMC3aeHbGiShvZsx4wMYSjH8e7Vrhbu6irwCzVBApESjbUdpWWmEnhathWu1jo+siFUiRAAxm9qyJNg/wOZqqzL/dL/q8PkcRU5oUKEpUge71M3ej2/7CPqpdVwuMoTvoB+ZOT4YeGyxMvHmbrxlFzGOHOijtzN+u1TQNatX2XBuzZNQ1K+s2CXkPIZo7s6JgZyvaBevYtxPvYLw4z9mR7K2vaF18UYH9Z9GNUUeayffKC73PYc=
.       28167   IN      DNSKEY  256 3 8 AwEAAZ5A7jOztf62cGqhPhutjnyl7KBjIsjbyTb8il+FsgbMUbO2NQHaSbatHdlOlqANncDwSIKZ9ryqd1+Dy1PoGzeTUv95vOJnVVJHlJu7xdavnUmPs+Mh2NV7hDlTTwPn5uXgFxAaxoO9M/YIAC92GryCLjoJEg9JzeevkktEM/sFpmRv4I5jQtlLyRqVbnCzcWpi04XaVLxRKvURkd/Mdb/2RQS3MYvrkEBXuqtnAVBCf6Fx4sgBYOfYvbUuG2diLnGJW/MXvFpctZgQ76+3FwMqAZfR9k5bohL7AF3+jqz4MUiootYoh5koyt7VEnUULxxy6U5PINTGgOC26f3zZuk=

`

	zone_new := `
.                       86400   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2 1800 900 604800 86400
.       28167   IN      DNSKEY  257 3 8 AwEAAa96jeuknZlaeSrvyAJj6ZHv28hhOKkx3rLGXVaC6rXTsDc449/cidltpkyGwCJNnOAlFNKF2jBosZBU5eeHspaQWOmOElZsjICMQMC3aeHbGiShvZsx4wMYSjH8e7Vrhbu6irwCzVBApESjbUdpWWmEnhathWu1jo+siFUiRAAxm9qyJNg/wOZqqzL/dL/q8PkcRU5oUKEpUge71M3ej2/7CPqpdVwuMoTvoB+ZOT4YeGyxMvHmbrxlFzGOHOijtzN+u1TQNatX2XBuzZNQ1K+s2CXkPIZo7s6JgZyvaBevYtxPvYLw4z9mR7K2vaF18UYH9Z9GNUUeayffKC73PYc=
			`

	info1, err := FromZonefileString(zone_old)
	if err != nil {
		t.Error(err)
	}

	info2, _ := FromZonefileString(zone_new)
	if err != nil {
		t.Error(err)
	}

	changes := info2.changes(info1)
	if len(changes) != 1 {
		t.Error("Expected 1 change, got ", len(changes))
	}

	change := changes[0]
	message := change.TootMessage()
	if !strings.Contains(message, `. : DNSKEY removed (ZSK)`) {
		t.Error("expected key rollover message for removed zsk. got ", message)
	}

}

func TestDNSKEYChangeRemoveMixed(t *testing.T) {
	zone_old := `
.                       86400   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 1 1800 900 604800 86400
.       28167   IN      DNSKEY  257 3 8 AwEAAa96jeuknZlaeSrvyAJj6ZHv28hhOKkx3rLGXVaC6rXTsDc449/cidltpkyGwCJNnOAlFNKF2jBosZBU5eeHspaQWOmOElZsjICMQMC3aeHbGiShvZsx4wMYSjH8e7Vrhbu6irwCzVBApESjbUdpWWmEnhathWu1jo+siFUiRAAxm9qyJNg/wOZqqzL/dL/q8PkcRU5oUKEpUge71M3ej2/7CPqpdVwuMoTvoB+ZOT4YeGyxMvHmbrxlFzGOHOijtzN+u1TQNatX2XBuzZNQ1K+s2CXkPIZo7s6JgZyvaBevYtxPvYLw4z9mR7K2vaF18UYH9Z9GNUUeayffKC73PYc=
.       28167   IN      DNSKEY  257 3 8 AwEAAaz/tAm8yTn4Mfeh5eyI96WSVexTBAvkMgJzkKTOiW1vkIbzxeF3+/4RgWOq7HrxRixHlFlExOLAJr5emLvN7SWXgnLh4+B5xQlNVz8Og8kvArMtNROxVQuCaSnIDdD5LKyWbRd2n9WGe2R8PzgCmr3EgVLrjyBxWezF0jLHwVN8efS3rCj/EWgvIWgb9tarpVUDK/b58Da+sqqls3eNbuv7pr+eoZG+SrDK6nWeL3c6H5Apxz7LjVc1uTIdsIXxuOLYA4/ilBmSVIzuDWfdRUfhHdY6+cn8HFRm+2hM8AnXGXws9555KrUB5qihylGa8subX2Nn6UwNR1AkUTV74bU=
.       28167   IN      DNSKEY  256 3 8 AwEAAZ5A7jOztf62cGqhPhutjnyl7KBjIsjbyTb8il+FsgbMUbO2NQHaSbatHdlOlqANncDwSIKZ9ryqd1+Dy1PoGzeTUv95vOJnVVJHlJu7xdavnUmPs+Mh2NV7hDlTTwPn5uXgFxAaxoO9M/YIAC92GryCLjoJEg9JzeevkktEM/sFpmRv4I5jQtlLyRqVbnCzcWpi04XaVLxRKvURkd/Mdb/2RQS3MYvrkEBXuqtnAVBCf6Fx4sgBYOfYvbUuG2diLnGJW/MXvFpctZgQ76+3FwMqAZfR9k5bohL7AF3+jqz4MUiootYoh5koyt7VEnUULxxy6U5PINTGgOC26f3zZuk=

`

	zone_new := `
.                       86400   IN      SOA     a.root-servers.net. nstld.verisign-grs.com. 2 1800 900 604800 86400
.       28167   IN      DNSKEY  257 3 8 AwEAAa96jeuknZlaeSrvyAJj6ZHv28hhOKkx3rLGXVaC6rXTsDc449/cidltpkyGwCJNnOAlFNKF2jBosZBU5eeHspaQWOmOElZsjICMQMC3aeHbGiShvZsx4wMYSjH8e7Vrhbu6irwCzVBApESjbUdpWWmEnhathWu1jo+siFUiRAAxm9qyJNg/wOZqqzL/dL/q8PkcRU5oUKEpUge71M3ej2/7CPqpdVwuMoTvoB+ZOT4YeGyxMvHmbrxlFzGOHOijtzN+u1TQNatX2XBuzZNQ1K+s2CXkPIZo7s6JgZyvaBevYtxPvYLw4z9mR7K2vaF18UYH9Z9GNUUeayffKC73PYc=
			`

	info1, err := FromZonefileString(zone_old)
	if err != nil {
		t.Error(err)
	}

	info2, _ := FromZonefileString(zone_new)
	if err != nil {
		t.Error(err)
	}

	changes := info2.changes(info1)
	if len(changes) != 1 {
		t.Error("Expected 1 change, got ", len(changes))
	}

	change := changes[0]
	message := change.TootMessage()
	if !strings.Contains(message, `. : DNSKEY removed 
`) {
		t.Error("expected key rollover message for removed mixed keys. got ", message)
	}

}

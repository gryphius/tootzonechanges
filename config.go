package main

import (
	"encoding/json"
	"os"
)

var CONFIG *BotConfig

type BotConfig struct {
	ClientID     string `json:"mastodon_client_id"`
	ClientSecret string `json:"mastodon_client_secret"`
	Email        string `json:"mastodon_email"`
	Password     string `json:"mastodon_password"`
	Server       string `json:"mastodon_server"`
	XfrServer    string `json:"dns_xfr_server"`
	XfrPort      string `json:"dns_xfr_port"`
}

func loadConfig(path string) {
	b, err := os.ReadFile(path)
	check(err)
	config := &BotConfig{}
	err = json.Unmarshal(b, &config)
	check(err)
	CONFIG = config
}
